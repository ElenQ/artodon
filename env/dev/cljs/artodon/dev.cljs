(ns ^:figwheel-no-load artodon.dev
  (:require
    [artodon.core :as core]
    [devtools.core :as devtools]))


(enable-console-print!)

(devtools/install!)

(core/init!)
