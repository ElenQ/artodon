<a href="https://liberapay.com/ekaitz/donate">
  <img alt="Donate using Liberapay" src="https://liberapay.com/assets/widgets/donate.svg">
</a>

# Artodon

Artodon is a PoC of an art page fully built on top of Mastodon. It's only a
different client for the browser with a gallery look.

The idea of the project is to show the art made by Mastodon users in an
appealing interface like Artstation does. Its main goal is to make a showcase
made by Mastodon community to make people take all the artists at Mastodon
seriously.

It's an art project.

It's something practical.

Visit it here:  
[elenq.gitlab.io/artodon](http://elenq.gitlab.io/artodon)


## Status

I'm making this as an art project, as a different visualization of the images
and data, so it's made as a read-only webapp.

> It can be developed in the future but it's not the first idea.

It shows a grid of art pieces taken from Mastodon `#mastoart` tag and links
them to original author's post. Nothing else.

It could be nice to have a tool to search between different tags but it's not
even started at the moment.


## About the Art

The art shown by this project is 100% property of the authors.

This software only provides the tool to visualize the content on Mastodon
community but not the content itself. That's made by the authors and it's their
property.


## Boring stuff for developers

Artodon is made in ClojureScript because I wanted to learn it. This section
tells you how to develop stuff on it and such. Not very detailed at the moment.

### Development mode

To start the Figwheel compiler, navigate to the project folder and run the
following command in the terminal:

```
lein figwheel
```

Figwheel will automatically push cljs changes to the browser.  Once Figwheel
starts up, you should be able to open the `public/index.html` page in the
browser.

#### CORS and Stuff

This project has no back-end at the moment and consumes everything from an API.
By default, the `artodon.config` namespace is used to configure the instance
the app is consuming. At the moment
[mastodon.social](https://mastodon.social)'s API is being used which supports
CORS but if you want to point it to another instance you must take this point
in consideration.

If the API you want to consume doesn't have CORS headers activated you need to
set up a proxy. At the same time you want to run Figwheel to have all the live
coding stuff so you need some kind of magic.

We use Robohydra for that.

Installation:

``` bash
npm install robohydra -g    # may require sudo
```

Robohydra runner (and a plugin!) is included in the project, to run it simply
use:

``` bash
./robohydra_serve
```

The `robohydra_serve` points the mastoart server to
[mastodon.social](https://mastodon.social) by default, you can change the URL
in the file.

> WARNING: We are accessing someone's server. Don't break it. PLEASE.

With that and figwheel running in the background, you'll be able to access
Robohydra server (http://localhost:3000) and make your stuff on it.

### Building for production

```
lein clean
lein package
```

## LICENSE

Copyright (C) 2017-?  Ekaitz Zárraga ekaitz@elenq.tech

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program. If not, see \<http://www.gnu.org/licenses/>.

### Notes

`public/img/placeholder.png` image is designed by the [Mastodon
team](https://github.com/tootsuite/mastodon/) but has been edited to fit this
projects color scheme.

The project LICENSE has been moved to AGPL from MIT.


## Contributing

Take part, this project is for everyone, it's not something for myself, it's a
reason to try new things, don't be shy.

## But... I don't know the tools you are using...

To be honest, me neither. I'm learning.

Here you have a couple of links you can use to learn more about the tools we
use here:

- A guide about the framework we use for the rendering, called `Reagent`  
  https://purelyfunctional.tv/guide/reagent/

- A good resource for ClojureScript and JavaScript interoperability:  
  http://www.spacjer.com/blog/2014/09/12/clojurescript-javascript-interop/

- A book about Clojure I bought and changed my life:  
  https://www.braveclojure.com/

## Thanks and cheers

- Esteban Manchado [`@estebanm`](https://mastodon.social/@estebanm) for the
  help with Clojure, ClojureScript and RoboHydra and for being a great human
  being.

- Glocal AKA [`curator@mastodon.art`](https://mastodon.art/@curator) for all
  the support during the development and the space in his instance!
