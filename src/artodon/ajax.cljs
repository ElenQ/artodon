;; AJAX

(ns artodon.ajax
  (:require
    [cljs-http.client :as http]
    [artodon.config :refer [instance]]
    ))

(def baseurl (str "https://" instance "/api/v1/"))

(defn get-posts
  "Download posts from the selected tag"
  [tag last_id first_id limit]
  (http/get (str baseurl "/timelines/tag/" tag)
            {:with-credentials? false
             :query-params
              {:max_id last_id
               :since_id first_id
               :limit limit}}))

(defn get-post
  "Download an specific post"
  [id]
  (http/get (str baseurl "/statuses/" id)
            {:with-credentials? false}))

(defn get-context
  "Download context of a post"
  [id]
  (http/get (str baseurl "/statuses/" id "/context")
            {:with-credentials? false}))
