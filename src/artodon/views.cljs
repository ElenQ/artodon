(ns artodon.views
  (:require
    [reagent.core :as r]
    [secretary.core :as scr]
    [accountant.core :as acc]
    [artodon.config :as conf]
    [clojure.string :as string]))

(acc/configure-navigation! {:nav-handler  (fn [path] (scr/dispatch! path))
                            :path-exists? (fn [path] (scr/locate-route path))
                            :reload-same-path? true})

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; General stuff
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defn blank-link
  "Simple a element with target set to blank"
  [url content]
  [:a {:href url :target "_blank"} content])

(defn layers-icon
  "Layers icon in pure SVG"
  []
  [:svg {:xmlns "http://www.w3.org/2000/svg"
         :class "layers-icon"
         :width  8
         :height 8
         :viewBox "0 0 8 8"}
    [:path {:d "M0 0v4h4v-4h-4zm5 2v3h-3v1h4v-4h-1zm2 2v3h-3v1h4v-4h-1z"}]])

(defn fav-icon
  []
  [:span "Favs "])
(defn boost-icon
  []
  [:span "Boosts "])

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Gallery page
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defn gallery-el
  "Renders a post element with a picture, and the author's name and picture on
  top"
  [post]
  (let [account (:account post)
        media   (:media_attachments post)
        id      (:id post)
        url     (:url post)
        mousein (r/atom false)
        images  (filter #(= (:type %) "image") media)]

    (fn []
      [:div.gallery-item {:on-click #(acc/navigate! (str "#/post/" id))
                          :on-mouse-enter #(reset! mousein true)
                          :on-mouse-leave #(reset! mousein false)}
        [:div.gallery-content
          [:img.gallery-image {:src (:preview_url (first images))}]
          (when (< 1 (count images))
            [:div.media-list [layers-icon]])
          [:div.shadow {:class (when (not @mousein) "hidden")}
            [:div.account
              [blank-link (:url account)
                          [:img.avatar {:src (:avatar_static account)}]]
              [blank-link (:url account)
                          [:span.name (or (:display_name account)
                                          (:username account))]]]]]])))

(defn gallery
  "Creates a full gallery from a sequence of posts"
  []
  (let [node        (r/atom {})
        shootevent  #(.dispatchEvent js/document
                                     (js/CustomEvent. "gallery-scroll-end")) ]
    (fn [posts]
      [:div#gallery.gallery
        {:ref #(reset! node %)
         :on-scroll #(let [scrollHeight (.-scrollHeight @node)
                           scrollTop    (.-scrollTop    @node)
                           clientHeight (.-clientHeight @node)]
                       (when (> (/ clientHeight 10)
                                (- scrollHeight scrollTop clientHeight))
                            (shootevent)))}
        (for [p posts]
          ^{:key (:id p)}[gallery-el p])])))



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Post page
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defn media-column
  "Creates a media-column from posts media_attachments"
  [media]
  (let [images (filter #(= "image" (:type %)) media)
        videos (filter #(= "video" (:type %)) media)]
    [:div.media-column
      (for [i images]
        ^{:key (:id i)}
        [blank-link (:url i)
                    [:img.fullwidth {:src (:url i)
                                     :alt (:description (:meta i))}]])
      (for [v videos]
        ^{:key (:id v)}[:video.fullwidth
                         [:source {:src (:url v)}]])]))
(defn account-card
  "Creates an accoun-card from account"
  [account]
  (let [acct          (:acct account)
        acct_instance (first (string/split (:url account) "@"))]
  [:div.account-card
    [blank-link (:url account) [:img.avatar {:src (:avatar account)}]]
    [:div.user-info
      [blank-link (:url account)
        [:h2.username (or (:display_name account) (:username account))]]
      [:span.date (str "Since: " (.toDateString (js/Date. (:created_at account))))]
      [:p.about {:dangerouslySetInnerHTML {:__html (:note account)}}]
      [:a.follow {:href (str acct_instance "users/" (:username account) "/remote_follow")
                  :target "_blank"}
        "Follow this user"]]]))


(defn toot
  "Creates a toot for the data-column"
  [content date boosts favs app]
  [:div.toot
    [:div.tootcontent  {:dangerouslySetInnerHTML {:__html content}}]
    [:div.toot-info
      [:span.date (.toLocaleString (js/Date. date))] " · "
      (when app (str app " · "))
      [:span.toot-favs   [fav-icon]   favs]  " · "
      [:span.toot-boosts [boost-icon] boosts]]])


(defn thread-toot
  "Each toot in the thread"
  [{:keys [account content created_at]}]
    [:div.answer
     [:a {:href (:url account)}
       [:img.avatar {:src (:avatar account)}]]
     [:div.answer-body
       [blank-link (:url account)
         [:h4.username (or (:display_name account) (:username account))]]
       [:span {:dangerouslySetInnerHTML {:__html content}}]]])

(defn thread
  "Creates a thread from a bunch of answers"
  [answers]
  (when (not= 0 (count answers))
    [:div.thread
      (for [ans answers]
        ^{:key (:id ans)}
        [thread-toot ans])]))

(defn data-column
  "Creates a data-column from posts metadata"
  [post]
  (let [content (:content     post)
        date    (:created_at  post)
        account (:account     post)
        answers (:answers     post)
        app     (:name (:application post))
        boosts  (or (:reblogs_count     post) 0)
        favs    (or (:favourites_count  post) 0)]
    [:div.data-column
      [:div.toolbar
        [:button.exit {:on-click #(acc/navigate! "#/")} "X"] ]
      [account-card account]
      [toot content date boosts favs app]
      [thread answers]]))

(defn post
  "Creates a post-view from post"
  [post]
  [:div.post
    [media-column (:media_attachments post)]
    [data-column post]])


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; MAINVIEW
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defn mainview
  "Creates the main view, the gallery is always there but there are overlays
  on top."
  [data]
  (let
    [page            (:page data)
     post-data       (:post data)
     gallery-data    (:gallery data)]
    [:div
      [gallery gallery-data]
        (cond
          (= page "post")
            [post post-data]

          ; put more possible pages here
          )]))
