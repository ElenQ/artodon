(ns artodon.core
    (:require-macros [cljs.core.async.macros :refer [go]])
    (:require
      [reagent.core :as r]
      [cljs.core.async :refer [<!]]
      [artodon.views :refer [mainview]]
      [artodon.ajax :refer [get-posts get-post get-context]]
      [goog.events :as events]
      [goog.history.EventType :as HistoryEventType]
      [secretary.core :as secretary :refer-macros [defroute]])
    (:import goog.history.Html5History))



;; -------------------------
;; Data

(def state (r/atom {:page "gallery"}))
; :page     stores the current page: "gallery" / "post"
; :post     nil/post-data
; :gallery  always updated list of posts

(def querying (atom false))
(def tag (atom "mastoart"))

;; -------------------------
;; Download shit
(defn update-gallery
  "Add gallery to the state or update it"
  [last_id first_id limit tag]
  (go (let [response (<! (get-posts tag last_id first_id limit))]
      (swap! state
             (fn [state data]
               (assoc state
                      :gallery
                      (->> (into (:gallery state) data)
                           distinct
                           (sort-by :id)
                           reverse)))
             (filter (fn [el]
                       (some #(= (:type %) "image")
                             (:media_attachments el)))
                     (:body response)))
      (reset! querying false))))

(defn open-post
  "Add post to the state -> opens it"
  [id]
  (go (let [post    (<! (get-post     id))
            context (<! (get-context  id))]
        (swap! state
               (fn [state post ans]
                 (assoc state :post (assoc post :answers ans) :page "post"))
               (:body post)
               (:descendants (:body context))))))

(defn close-post
  "Remove post from the state -> closes it"
  []
  (swap! state
         #(dissoc %1 :post))
  (swap! state
         #(assoc %1 :page "gallery")))

(defn get-old-pictures
  [tag]
  (update-gallery (-> (:gallery @state) last :id)
                  nil
                  (if (first (:gallery @state)) 40 40)
                  tag))

(defn get-new-pictures
  [tag]
  (update-gallery nil
                  (-> (:gallery @state) first :id)
                  (if (first (:gallery @state)) 40 40)
                  tag))

;; -------------------------
;; Initialize app

(get-old-pictures @tag)

(defn create_view
  []
  [mainview @state])

(defn mount-root []
  (r/render [create_view] (.getElementById js/document "app")))


;; Get infinite scroll:
(.addEventListener
  js/document
  "gallery-scroll-end"
  (fn
    [e]
    (reset! querying true)
    (get-old-pictures @tag)))

;; Get new shit
(js/setInterval
  #(get-new-pictures @tag)
  20000)


;; -------------------------
;; Routes
(defroute "/" {:as params}
  (close-post))
(defroute "/post/:id" {:as params}
  (open-post (:id params)))
(secretary/set-config! :prefix "#")

;; -------------------------
;; History
;; must be called after routes have been defined
(defn hook-browser-navigation! []
  (doto (Html5History.)
        (events/listen
          HistoryEventType/NAVIGATE
          (fn [event]
              (secretary/dispatch! (.-token event))))
        (.setEnabled true)))

(defn init! []
  (hook-browser-navigation!)
  (mount-root))
